module.exports = {
    "port": process.env.PORT || 8080,
    "jwt_secret": "myS33!!creeeT",
    "jwt_expiration_in_seconds": 36000,
    "environment": "dev"
};